const App = require('express')()
const { body, validationResult } = require('express-validator')
const { Model: Division } = require('./db/models/division')
const { Model: Employee } = require('./db/models/employee')
const { Model: Visitor } = require('./db/models/visitor')

App.use('/api/*', (req, res, next) => {
  // TODO authentication
  next()
})

/**
 * Divisions CRUD endpoints
 */
const divisionSanitizer = [
  body('name').not().isEmpty().trim(),
  body('sortOrder').not().isEmpty()
]

App.get('/api/divisions', async (req, res) => {
  let response
  try {
    response = await Division.find({}).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }
  return res.status(200).send(response)
})

App.get('/api/division/:id', async (req, res) => {
  const id = req.params.id
  let response
  try {
    response = await Division.findOne({ _id: id }).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }
  if (!response) {
    return res.status(404).send({ msg: `Division ${id} not found` })
  }
  return res.status(200).send(response)
})

App.post('/api/division', ...divisionSanitizer, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send({ errors: errors.array() });
  }

  const data = req.body
  const NewDivision = new Division(data)

  try {
    await NewDivision.validate()
  } catch (err) {
    return res.status(400).send({ msg: err.errors })
  }

  try {
    await NewDivision.save()
  } catch (err) {
    return res.status(500).send({ msg: `Error saving data :: ${err.message}` })
  }

  return res.status(200).send(NewDivision)
})

App.put('/api/division/:id', ...divisionSanitizer, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send({ errors: errors.array() });
  }

  const id = req.params.id
  const data = req.body

  let OldDivision
  try {
    OldDivision = await Division.findOne({ _id: id }).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }

  if (!OldDivision) {
    return res.status(404).send({ msg: `Division ${id} not found` })
  }

  OldDivision.set(data)

  try {
    await OldDivision.validate()
  } catch (err) {
    return res.status(400).send({ msg: err.errors })
  }

  try {
    await OldDivision.save()
  } catch (err) {
    return res.status(500).send({ msg: `Error saving data :: ${err.message}` })
  }

  return res.status(200).send(OldDivision)
})

App.delete('/api/division/:id', async (req, res) => {
  const id = req.params.id

  let response
  try {
    response = await Division.findOne({ _id: id }).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }

  if (!response) {
    return res.status(404).send({ msg: `Division ${id} not found` })
  }

  response = undefined
  try {
    response = await Division.deleteOne({ _id: id })
  } catch (err) {
    return res.status(500).send({ msg: `Error deleting data :: ${err.message}` })
  }

  if (!response || !response.deletedCount) {
    return res.status(500).send({ msg: `Error deleting data :: Nothing was deleted` })
  }

  return res.status(200).send({ msg: `Division ${id} deleted` })
})

/**
 * Employee CRUD endpoints
 */
const employeeSanitizer = [
  body('lastName').not().isEmpty().trim(),
  body('firstName').not().isEmpty().trim(),
  body('title').optional().trim(),
  body('honorific').optional().trim(),
  body('phone').trim()
]

App.get('/api/employees', async (req, res) => {
  const includeInactive = req.query.includeInactive === 'true'

  let response
  try {
    const query = includeInactive ? {} : { isActive: true }
    response = await Employee.find(query).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }
  return res.status(200).send(response)
})

App.get('/api/employee/:id', async (req, res) => {
  const id = req.params.id
  let response
  try {
    response = await Employee.findOne({ _id: id }).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }
  if (!response) {
    return res.status(404).send({ msg: `Employee ${id} not found` })
  }
  return res.status(200).send(response)
})

App.post('/api/employee', ...employeeSanitizer, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send({ errors: errors.array() });
  }

  const data = req.body
  const NewEmployee = new Employee(data)

  try {
    await NewEmployee.validate()
  } catch (err) {
    return res.status(400).send({ msg: err.errors })
  }

  try {
    await NewEmployee.save()
  } catch (err) {
    return res.status(500).send({ msg: `Error saving data :: ${err.message}` })
  }

  return res.status(200).send(NewEmployee)
})

App.put('/api/employee/:id', ...employeeSanitizer, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send({ errors: errors.array() });
  }

  const id = req.params.id
  const data = req.body

  let OldEmployee
  try {
    OldEmployee = await Employee.findOne({ _id: id }).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }

  if (!OldEmployee) {
    return res.status(404).send({ msg: `Employee ${id} not found` })
  }

  OldEmployee.set(data)

  try {
    await OldEmployee.validate()
  } catch (err) {
    return res.status(400).send({ msg: err.errors })
  }

  try {
    await OldEmployee.save()
  } catch (err) {
    return res.status(500).send({ msg: `Error saving data :: ${err.message}` })
  }

  return res.status(200).send(OldEmployee)
})

App.delete('/api/employee/:id', async (req, res) => {
  const id = req.params.id

  let response
  try {
    response = await Employee.findOne({ _id: id }).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }

  if (!response) {
    return res.status(404).send({ msg: `Employee ${id} not found` })
  }

  response = undefined
  try {
    response = await Employee.deleteOne({ _id: id })
  } catch (err) {
    return res.status(500).send({ msg: `Error deleting data :: ${err.message}` })
  }

  if (!response || !response.deletedCount) {
    return res.status(500).send({ msg: `Error deleting data :: Nothing was deleted` })
  }

  return res.status(200).send({ msg: `Employee ${id} deleted` })
})

/**
 * Visitor CRUD endpoints
 */
const visitorSanitizer = [
  body('lastName').optional().trim(),
  body('firstName').optional().trim(),
  body('phone').optional().trim(),
  body('designation').optional().trim(),
  body('purpose').optional().trim(),
  body('visitingDivision').optional().trim(),
  body('visitingEmployee').optional().trim()
]

App.get('/api/visitors', async (req, res) => {
  const includeDeleted = req.query.includeDeleted === 'true'

  let response
  try {
    const query = includeDeleted ? {} : { deletedAt: { $exists: false } }
    response = await Visitor.find(query).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }
  return res.status(200).send(response)
})

App.get('/api/visitor/:id', async (req, res) => {
  const id = req.params.id
  let response
  try {
    response = await Visitor.findOne({ _id: id }).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }
  if (!response) {
    return res.status(404).send({ msg: `Visitor ${id} not found` })
  }
  return res.status(200).send(response)
})

App.post('/api/visitor', ...visitorSanitizer, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send({ errors: errors.array() });
  }

  const data = req.body
  const NewVisitor = new Visitor(data)

  try {
    await NewVisitor.validate()
  } catch (err) {
    return res.status(400).send({ msg: err.errors })
  }

  try {
    await NewVisitor.save()
  } catch (err) {
    return res.status(500).send({ msg: `Error saving data :: ${err.message}` })
  }

  return res.status(200).send(NewVisitor)
})

App.put('/api/visitor/:id', ...visitorSanitizer, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send({ errors: errors.array() });
  }

  const id = req.params.id
  const data = req.body

  let OldVisitor
  try {
    OldVisitor = await Visitor.findOne({ _id: id }).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }

  if (!OldVisitor) {
    return res.status(404).send({ msg: `Visitor ${id} not found` })
  }

  OldVisitor.set(data)
  if (typeof data.deletedAt !== 'undefined' && data.deletedAt === null) {
    OldVisitor.set({ deletedAt: undefined })
  }

  try {
    await OldVisitor.validate()
  } catch (err) {
    return res.status(400).send({ msg: err.errors })
  }

  try {
    await OldVisitor.save()
  } catch (err) {
    return res.status(500).send({ msg: `Error saving data :: ${err.message}` })
  }

  return res.status(200).send(OldVisitor)
})

App.delete('/api/visitor/:id', async (req, res) => {
  const id = req.params.id

  let response
  try {
    response = await Visitor.findOne({ _id: id }).exec()
  } catch (err) {
    return res.status(500).send({ msg: `Error fetching data :: ${err.message}` })
  }

  if (!response) {
    return res.status(404).send({ msg: `Visitor ${id} not found` })
  }

  response = undefined
  try {
    response = await Visitor.deleteOne({ _id: id })
  } catch (err) {
    return res.status(500).send({ msg: `Error deleting data :: ${err.message}` })
  }

  if (!response || !response.deletedCount) {
    return res.status(500).send({ msg: `Error deleting data :: Nothing was deleted` })
  }

  return res.status(200).send({ msg: `Visitor ${id} deleted` })
})

module.exports = App
