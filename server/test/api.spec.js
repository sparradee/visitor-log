const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('express')()
const { MongoMemoryServer } = require('mongodb-memory-server')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const api = require('../api.js')

chai.use(chaiHttp)
app.use(bodyParser.json(), api)
const { expect } = chai

const { Model: Visitor } = require('../db/models/visitor')
const { Model: Employee } = require('../db/models/employee')
const { Model: Division } = require('../db/models/division')

describe('Testing the visitor-Log API', () => {

  before(async () => {
    const mongoDb = await MongoMemoryServer.create()
    await mongoose.connect(mongoDb.getUri(), {})
  })

  it('should verify connection to an empty database', async () => {
    const visitors = await Visitor.find()
    const employees = await Employee.find()
    const divisions = await Division.find()
    expect(visitors).to.be.an('array').with.lengthOf(0)
    expect(employees).to.be.an('array').with.lengthOf(0)
    expect(divisions).to.be.an('array').with.lengthOf(0)
  })

   describe('Test API endpoints', () => {

    describe('Employee', () => {
      const employeeList = [
        { firstName: 'Adam', lastName: 'Adamford', title: 'Mr' },
        { firstName: 'Betty', lastName: 'Bettanski', title: 'Mrs' },
        { firstName: 'Charlie', lastName: 'Charleston', title: 'Dr' }
      ]

      it('should create three employees', async () => {
        for (const employeeData of employeeList) {
          await chai.request(app)
            .post('/api/employee')
            .send(employeeData)
            .then((res) => {
              expect(res).to.have.status(200)
              expect(res.body).to.be.an('object')
              expect(res.body.firstName).to.equal(employeeData.firstName)
              expect(res.body.firstName).to.equal(employeeData.firstName)
              expect(res.body.title).to.equal(employeeData.title)
              employeeData._id = res.body._id // Add the ObjectId to our test array
            })
            .catch((err) => {
              console.error(err)
            })
        }
      })

      it('should fail to create an employee missing required data', (done) => {
        chai.request(app)
          .post('/api/employee')
          .send({ lastName: 'MissingFirstName' })
          .end((err, res) => {
            expect(res).to.have.status(400)
            expect(res.body).to.be.an('object').with.property('msg')
            done()
          })
      })

      it('should get a list of three employees', (done) => {
        chai.request(app)
          .get('/api/employees')
          .end((err, res) => {
            expect(res).to.have.status(200)
            expect(res.body).to.be.an('array').with.lengthOf(3)
            done()
          })
      })

      it('should update an employee', (done) => {
        chai.request(app)
          .put(`/api/employee/${employeeList[0]._id}`)
          .send({ lastName: 'Adamsson' })
          .end((err, res) => {
            expect(res).to.have.status(200)
            expect(res.body).to.be.an('object')
            expect(res.body.firstName).to.equal(employeeList[0].firstName)
            expect(res.body.lastName).to.not.equal(employeeList[0].lastName)
            expect(res.body.lastName).to.equal('Adamsson')
            done()
          })
      })

      it('should delete an employee', async () => {
        await chai.request(app)
          .delete(`/api/employee/${employeeList[1]._id}`)
          .then((res) => {
            expect(res).to.have.status(200)
            expect(res.body).to.be.an('object').with.property('msg')
          })

        // Verify what's in the database
        const employees = await Employee.find()
        expect(employees).to.be.an('array').with.lengthOf(2)
        expect(employees.find((i) => i.firstName === employeeList[1].firstName)).to.be.undefined
      })

    })

    describe('Division', () => {
      const divisionList = [
        { name: 'First' },
        { name: 'Second' }
      ]

      it('should create two divisions', async () => {
        for (const divisionData of divisionList) {
          await chai.request(app)
            .post('/api/division')
            .send(divisionData)
            .then((res) => {
              expect(res).to.have.status(200)
              expect(res.body).to.be.an('object')
              expect(res.body.name).to.equal(divisionData.name)
              divisionData._id = res.body._id // Add the ObjectId to our test array
            })
            .catch((err) => {
              console.error(err)
            })
        }
      })

      it('should fail to create an division missing required data', (done) => {
        chai.request(app)
          .post('/api/division')
          .send({ garbage: 'in' })
          .end((err, res) => {
            expect(res).to.have.status(400)
            expect(res.body).to.be.an('object').with.property('msg')
            done()
          })
      })

      it('should get a list of two divisions', (done) => {
        chai.request(app)
          .get('/api/divisions')
          .end((err, res) => {
            expect(res).to.have.status(200)
            expect(res.body).to.be.an('array').with.lengthOf(2)
            done()
          })
      })

      it('should update an division', (done) => {
        chai.request(app)
          .put(`/api/division/${divisionList[0]._id}`)
          .send({ name: 'Updated First' })
          .end((err, res) => {
            expect(res).to.have.status(200)
            expect(res.body).to.be.an('object')
            expect(res.body.name).to.not.equal(divisionList[0].name)
            expect(res.body.name).to.equal('Updated First')
            done()
          })
      })

      it('should delete an division', async () => {
        await chai.request(app)
          .delete(`/api/division/${divisionList[1]._id}`)
          .then((res) => {
            expect(res).to.have.status(200)
            expect(res.body).to.be.an('object').with.property('msg')
          })

        // Verify what's in the database
        const divisions = await Division.find()
        expect(divisions).to.be.an('array').with.lengthOf(1)
        expect(divisions.find((i) => i.name === divisionList[0].name)).to.be.undefined
      })

    })

    describe('Visitor', () => {
      const visitorList = [
        { firstName: 'Zenith', lastName: 'Zenithing', phone: '123-456-7890', designation: 'Visitor', purpose: 'Visiting', visiting: 'First' },
        { firstName: 'Yennifer', lastName: 'of Vengerberg', phone: '111-222-3333', designation: 'Contractor', purpose: 'Witches At large', visiting: 'Charlie Charleston' }
      ]

      it('should create two visitors', async () => {
        for (const visitorData of visitorList) {
          await chai.request(app)
            .post('/api/visitor')
            .send(visitorData)
            .then((res) => {
              expect(res).to.have.status(200)
              expect(res.body).to.be.an('object')
              expect(res.body.firstName).to.equal(visitorData.firstName)
              expect(res.body.firstName).to.equal(visitorData.firstName)
              expect(res.body.fullName).to.equal(`${visitorData.firstName} ${visitorData.lastName}`)
              // @todo: test the rest of the properties
              expect(res.body.phone).to.equal(visitorData.phone.replace(/([^0-9])/ig, ''))
              visitorData._id = res.body._id // Add the ObjectId to our test array
            })
            .catch((err) => {
              console.error(err)
            })
        }
      })

      it('should fail to create an visitor missing required data', (done) => {
        chai.request(app)
          .post('/api/visitor')
          .send({ garbage: 'in' })
          .end((err, res) => {
            expect(res).to.have.status(400)
            expect(res.body).to.be.an('object').with.property('msg')
            done()
          })
      })

      it('should get a list of two visitors', (done) => {
        chai.request(app)
          .get('/api/visitors')
          .end((err, res) => {
            expect(res).to.have.status(200)
            expect(res.body).to.be.an('array').with.lengthOf(2)
            done()
          })
      })

      it('should update an visitor', (done) => {
        chai.request(app)
          .put(`/api/visitor/${visitorList[0]._id}`)
          .send({ lastName: 'Zorrander' })
          .end((err, res) => {
            expect(res).to.have.status(200)
            expect(res.body).to.be.an('object')
            expect(res.body.lastName).to.not.equal(visitorList[0].lastName)
            expect(res.body.lastName).to.equal('Zorrander')
            done()
          })
      })

      it('should delete an visitor', async () => {
        await chai.request(app)
          .delete(`/api/visitor/${visitorList[1]._id}`)
          .then((res) => {
            expect(res).to.have.status(200)
            expect(res.body).to.be.an('object').with.property('msg')
          })

        // Verify what's in the database
        const visitors = await Visitor.find()
        expect(visitors).to.be.an('array').with.lengthOf(1)
        expect(visitors.find((i) => i._id === visitorList[0]._id)).to.be.undefined
      })

    })
  })

})
