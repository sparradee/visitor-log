const App = require('express')()
const { urlencoded, json }  = require('body-parser')
const cors = require('cors')
const API = require('./api')
const CONFIG = require('./config.json')
require('./db')

const apiPort = parseInt(CONFIG.API_PORT || 3000)

App.use(urlencoded({ extended: true, limit: '1mb' }), cors(), json({ limit: '1mb' }))

App.use(API)

/**
 * For any URL that doesn't begin with /api, we redirect that request into the
 * frontend/build/ folder; this is for Production use. While in Development, this
 * won't function as expected - development mode is configured to run this back-
 * end API on port 3000, and the front-end GUI on port 3001.
 */
App.get(/^\/(?!api(.*))/, (req, res) => {
  return res.sendFile(`${__dirname}/frontend/build${req.path}`)
})

App.listen(apiPort, () => console.log(`Server running on port ${apiPort}`))
