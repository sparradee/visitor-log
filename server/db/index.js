const mongoose = require('mongoose')
const { MongoMemoryServer } = require('mongodb-memory-server')
const CONFIG = require('../config.json')

const DB = {
  connect: async () => {
    let mongoURL = `mongodb://${CONFIG.DB_HOST}:${CONFIG.DB_PORT}/${CONFIG.DB_NAME}`
    if (process.env.USE_MEMORY_SERVER) {
      const mongoDB = await MongoMemoryServer.create()
      mongoURL = mongoDB.getUri()
    }

    mongoose
      .connect(mongoURL, { useNewUrlParser: true })
      .catch((err) => {
        console.error('Connection error', err.message)
      })

    if (process.env.LOG_LEVEL === 'silly' || process.env.LOG_LEVEL === 'debug') {
      mongoose.set('debug', true)
    }

    mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'))
    console.log(`Database connected on ${mongoURL}`)

    return mongoose.connection
  }
}

module.exports = DB.connect()
