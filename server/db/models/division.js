const { Schema, model } = require('mongoose')

const DivisionSchema = new Schema({
  name: { type: String, required: true },
  sortOrder: { type: Number }
}, { collection: 'divisions', strict: true, timestamps: true, toJSON: { getters: true } })

DivisionSchema.index({ lastName: 1 })

const DivisionModel = model('Division', DivisionSchema)

module.exports = { Model: DivisionModel, Schema: DivisionSchema }
