const { Schema, Types, model } = require('mongoose')

const EmployeeSchema = new Schema({
  division: { type: [Types.ObjectId], ref: 'Division' },
  firstName: { type: String, required: true },
  honorific: { type: String, default: undefined },
  isActive: { type: Boolean, default: true },
  lastName: { type: String, required: true },
  title: { type: String, default: undefined },
}, { collection: 'employees', strict: true, timestamps: true, toJSON: { getters: true } })

EmployeeSchema.index({ lastName: 1 })
EmployeeSchema.index({ division: 1 })

EmployeeSchema.virtual('fullName').get((val, virt, doc) => `${doc.honorific ? `${doc.honorific} ` : ''}${doc.firstName} ${doc.lastName}${doc.title ? `, ${doc.title}` : ''}`)
EmployeeSchema.virtual('fullLastName').get((val, virt, doc) => `${doc.honorific ? `${doc.honorific} ` : ''}${doc.lastName}, ${doc.firstName}${doc.title ? `, ${doc.title}` : ''}`)
EmployeeSchema.virtual('fullNameNoTitle').get((val, virt, doc) => `${doc.honorific ? `${doc.honorific} ` : ''}${doc.firstName} ${doc.lastName}`)

EmployeeSchema.post('find', async (docs) => {
  for (const doc of docs) {
    await doc.populate('division')
  }
  return docs
})

const EmployeeModel = model('Employee', EmployeeSchema)

module.exports = { Model: EmployeeModel, Schema: EmployeeSchema }
