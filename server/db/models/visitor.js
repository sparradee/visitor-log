const { Schema, model } = require('mongoose')

const VisitorSchema = new Schema({
  lastName: { type: String, required: true },
  firstName: { type: String, required: true },
  phone: { type: String, default: undefined, set: (val) => val ? val.replace(/([^0-9#])/ig, '') : val },
  designation: { type: String, enum: ['Visitor', 'Contractor'], default: 'Visitor', required: true},
  purpose: { type: String, default: ''},
  visitingDivision: { type: String, default: undefined },
  visitingEmployee: { type: String, default: undefined },
  deletedAt: { type: Date, default: undefined },
  editedAt: { type: Date, default: undefined },
  photo: { type: String, default: undefined }
}, { collection: 'visitors', strict: true, timestamps: true, toJSON: { getters: true } })

VisitorSchema.index({ lastName: 1 })
VisitorSchema.virtual('fullName').get((val, virt, doc) => `${doc.firstName} ${doc.lastName}`)
VisitorSchema.virtual('maskedPhone').get((val, virt, doc) => `${doc.phone.substr(-4).padStart(10, '*')}`)

const VisitorModel = model('Visitor', VisitorSchema)

module.exports = { Model: VisitorModel, Schema: VisitorSchema }
