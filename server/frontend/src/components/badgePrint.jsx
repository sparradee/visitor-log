import React from 'react'
import Day from 'dayjs'
import { Textfit } from 'react-textfit'
import { Link } from 'react-router-dom'
import { decode } from 'html-entities'

function BadgePrint (props) {
  const visitor = props.visitor
  const doneFunction = props.done || null

  return (
    <div className="d-print-block p-0 m-0">
      <div className="badge-wrapper position-absolute p-0 m-0">
        <div className={`badge-area ${visitor.designation} p-0 m-0`}>
          { visitor.photo ? (
            <div className="badge-photo-area">
              <div className="badge-photo-helper">
                <img src={visitor.photo} className="badge-photo" />
              </div>
            </div>
          ) : null}

          <div className="badge-name overflow-hidden">
            <Textfit mode="single" min={24} max={40}>
              {decode(visitor?.fullName || `${visitor?.firstName} ${visitor?.lastName}`)}
            </Textfit>
          </div>
          { visitor?.purpose ? (
            <div className="badge-purpose overflow-hidden">
              <Textfit mode="single" min={14} max={18}>
                {decode(visitor?.purpose || '')}
              </Textfit>
            </div>
          ) : null}
          { visitor?.visitingDivision ? (
            <div className="badge-visiting">
              <div className="visiting-division overflow-hidden">
                <Textfit mode="single" min={14} max={18}>
                  {decode(visitor.visitingDivision)}
                </Textfit>
              </div>
              <div className="visiting-employee overflow-hidden">
                <Textfit mode="single" min={14} max={18}>
                  {decode(visitor.visitingEmployee)}
                </Textfit>
              </div>
            </div>
          ) : null}
          <div className="badge-timestamp overflow-hidden">
            Printed: {decode(Day().format('DD MMM YYYY [&nbsp;] HH:mm a'))}
          </div>
          <div className="badge-validity overflow-hidden">
            Valid: {Day(visitor.createdAt).format('DD MMM YYYY')}
          </div>
        </div>
        { doneFunction ? (
          <div className="d-print-none text-center my-3">
            <Link to="/home" className="btn btn-lg btn-primary" onClick={doneFunction}>
              <i className="fa fa-fw fa-check"></i>
              Done
            </Link>
          </div>
        ) : null}
      </div>
    </div>
  )
}

export default BadgePrint
