import React from 'react'
import { Spinner } from 'react-bootstrap'

function PageMask (props) {
  const show = props.show || false

  return !show ? '' : (
    <div className="position-absolute" style={{top: 0, left: 0, width: '100%', height: '100%', backgroundColor: 'rgba(255, 255, 255, 0.75)', zIndex: 5}}>
      <div className="position-absolute top-50 start-50">
        <Spinner animation="border" />
      </div>
    </div>
  )
}

export default PageMask
