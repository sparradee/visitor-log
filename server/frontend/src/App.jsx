import React from 'react'
import { HashRouter as Router, Routes, Route } from 'react-router-dom'
import SiteTemplate from './pages/siteTemplate'
import HomePage from './pages/homePage'
import SignIn from './pages/signIn'
import SignOut from './pages/signOut'
import StaffLoginPage from './pages/staffLoginPage'

import 'bootstrap/dist/js/bootstrap.min.js'
import '@fortawesome/fontawesome-free/css/all.min.css'
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import './app.css'

function App () {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" element={<SiteTemplate />}>
            <Route path="/home" element={<HomePage />} />
            <Route path="/signin" element={<SignIn />} />
            <Route path="/signout" element={<SignOut />} />
            <Route path="/staff" element={<StaffLoginPage />} />
            <Route path="" element={<HomePage />} />
          </Route>
        </Routes>
      </Router>
    </div>
  )
}

export default App;
