import axios from 'axios'
import CONFIG from '../config.json'

const API = axios.create({
  baseURL: `http://localhost:${CONFIG.API_PORT}/api`
})

const apis = {
  visitor: {
    create: (data) => API.post('/visitor', data),
    get: (id) => API.get(`/visitor/${id}`),
    getAll: (data) => API.get(`/visitors${data ? `?${(new URLSearchParams(data)).toString()}` : ''}`),
    update: (id, data) => API.put(`/visitor/${id}`, data),
    delete: (id) => API.delete(`/visitor/${id}`)
  },

  division: {
    create: (data) => API.post('/division', data),
    get: (id) => API.get(`/division/${id}`),
    getAll: (data) => API.get('/divisions', data),
    update: (id, data) => API.put(`/division/${id}`, data),
    delete: (id) => API.delete(`/division/${id}`)
  },

  employee: {
    create: (data) => API.post('/employee', data),
    get: (id) => API.get(`/employee/${id}`),
    getAll: (data) => API.get(`/employees${data ? `?${(new URLSearchParams(data)).toString()}` : ''}`),
    update: (id, data) => API.put(`/employee/${id}`, data),
    delete: (id) => API.delete(`/employee/${id}`)
  },
}

export default apis
