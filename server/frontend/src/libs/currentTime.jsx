import React, { useState } from 'react'
import Day from 'dayjs'

function CurrentTime () {
  const [currentTime, setCurrentTime] = useState()
  setInterval(() => setCurrentTime(Day().format('DD MMM YYYY / HH:mm:ss'), 500))

  return (
    <span>
      {currentTime}
    </span>
  )
}

export default CurrentTime
