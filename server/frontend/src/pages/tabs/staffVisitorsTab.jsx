import React, { useState, useEffect } from 'react'
import { decode } from 'html-entities'
import { Modal, Tooltip, OverlayTrigger } from 'react-bootstrap'
import NumberFormat from 'react-number-format'
import API from '../../libs/api'
import PageMask from '../../components/pageMask'
import BadgePrint from '../../components/badgePrint'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from "react-bootstrap-table2-paginator";
import RelativeTime from 'dayjs/plugin/relativeTime'
import Day from 'dayjs'
import { CSVLink } from 'react-csv'
import CONFIG from '../../config.json'

Day.extend(RelativeTime)

function StaffVisitorsTab (props) {

  const [includeDeleted, setIncludeDeleted] = useState(false)
  const [visitors, setVisitors] = useState([])
  const [maskPage, setMaskPage] = useState(false)
  const [showBadgeForVisitor, setVisitorBadge] = useState(null)
  const [editingVisitor, setEditingVisitor] = useState(null)
  const [divisionList, setDivisionList] = useState([])
  const [employeeList, setEmployeeList] = useState([])
  const [filteredEmployeeList, setFilteredEmployeeList] = useState([])
  const [purgingVisitor, setPurgingVisitor] = useState(null)

  useEffect(() => {
    API.division
      .getAll()
      .then((res) => setDivisionList(res.data.sort((a, b) => a.name > b.name ? 1 : -1)))

    API.employee
      .getAll({ includeInactive: false })
      .then((res) => setEmployeeList(res.data.sort((a, b) => a.lastName > b.lastName ? 1 : -1)))

    getVisitors()

  }, [])

  const getVisitors = (forceIncludeDeleted = null) => {
    setMaskPage(true)
    API.visitor.getAll({ includeDeleted: forceIncludeDeleted === null ? includeDeleted : !!forceIncludeDeleted }).then((res) => {
      setVisitors(res.data)
      setMaskPage(false)
    })
  }

  const csvOutput = () => {
    const flatData = JSON.parse(JSON.stringify(visitors))
    for (const entry of flatData) {
      ['_id', 'id', '__v', 'maskedPhone', 'editedAt', 'photo'].forEach((i) => delete entry[i])
      entry.createdAt = Day(entry.createdAt).format('YYYY-MM-DD HH:mm:ss')
      entry.updatedAt = entry.updatedAt ? Day(entry.updatedAt).format('YYYY-MM-DD HH:mm:ss') : ''

      for (const prop in entry) {
        entry[prop] = decode(entry[prop])
      }
    }
    return flatData
  }

  const signOutVisitor = (visitor) => {
    setMaskPage(true)
    API.visitor.update(visitor._id, { deletedAt: new Date() }).then(() => {
      getVisitors()
    }).catch((err) => {
      setMaskPage(false)
      console.error(err)
    })
  }

  const signInVisitor = (visitor) => {
    setMaskPage(true)
    API.visitor.update(visitor._id, { deletedAt: null }).then(() => {
      getVisitors()
    }).catch((err) => {
      setMaskPage(false)
      console.error(err)
    })
  }

  const printVisitorBage = (visitor) => {
    setVisitorBadge(visitor)
    setTimeout(() => {
      window.print()
    }, 50)
  }

  const deleteVisitorRecord = (visitor) => {
    setPurgingVisitor(visitor)
  }

  const cancelDelete = () => {
    setPurgingVisitor(null)
  }

  const processDelete = () => {
    setMaskPage(true)
    API.visitor.delete(purgingVisitor._id).then(() => {
      setPurgingVisitor(null)
      getVisitors()
    }).catch((err) => {
      setMaskPage(false)
      console.error(err)
    })
  }

  const editVisitorRecord = (visitor) => {
    if (visitor.visitingDivision) {
      setFilteredEmployeeList(employeeList.filter((employee) => employee?.division?.length ? !!employee.division.find((div) => div.name === visitor.visitingDivision) : false))
    }
    setEditingVisitor(visitor)
  }

  const saveVisitorReady = () => {
    return editingVisitor.designation && editingVisitor.lastName && editingVisitor.firstName && editingVisitor.purpose && editingVisitor.phone && editingVisitor.visitingDivision
  }

  const saveEditingVisitor = () => {
    setMaskPage(true)
    const dataToSubmit = Object.assign({}, editingVisitor, { editedAt: new Date() })
    API.visitor.update(editingVisitor._id, dataToSubmit)
      .then((res) => {
        setEditingVisitor(null)
        getVisitors()
      })
      .catch((err) => {
        setMaskPage(false)
        console.error(err)
      })
  }

  const changeDivision = (evt) => {
    const divisionName = evt.target.value
    setEditingVisitor({ ...editingVisitor, visitingDivision: divisionName, visitingEmployee: '' })
    if (employeeList.length) {
      setFilteredEmployeeList(employeeList.filter((employee) => employee?.division?.length ? !!employee.division.find((div) => div.name === divisionName) : false))
    }
  }

  const formatPhone = (input) => {
    const pattern = /^(?<area>[0-9]{3})(?<exchange>[0-9]{3})(?<subscriber>[0-9]{4})(?<extension>[0-9]{1,5})?$/
    const parsed = input.match(pattern)?.groups
    return parsed ? `(${parsed.area}) ${parsed.exchange}-${parsed.subscriber}${parsed.extension ? ` #${parsed.extension}` : ''}` : '--'
  }

  const tableColumns = [
    CONFIG.WEBCAM ? {
      text: 'Photo',
      dataField: 'photo',
      headerStyle: {
        width: '100px'
      },
      formatter: (col) => !!col ? (
        <OverlayTrigger placement="top" delay={{ show: 250, hide: 150 }} overlay={(
            <Tooltip>
              <img src={col} className="img img-fluid" />
            </Tooltip>
          )}
          >
          <img src={col} className="img img-fluid" />
        </OverlayTrigger>
      ) : (
        <div className="text-center fa-stack fa-2x" style={{ opacity: 0.33 }}>
          <i className="fa fa-camera fa-stack-1x"></i>
          <i className="fa fa-ban fa-stack-2x" style={{ color: 'Tomato' }}></i>
        </div>
      )
    } : { dataField: 'photo', hidden: true },
    {
      text: 'Designation',
      dataField: 'designation',
      sort: true,
      headerStyle: {
        width: '135px'
      },
      formatter: (col) => decode(col)
    },
    {
      text: 'Name / Phone',
      dataField: 'fullName',
      sort: true,
      formatter: (col, row) => (
        <div>
          <div>
            {decode(col)}
          </div>
          <div>
            <small>
              {formatPhone(row.phone)}
            </small>
          </div>
        </div>
      )
    },
    {
      text: 'Purpose / Company',
      dataField: 'purpose',
      sort: true,
      formatter: (col) => decode(col)
    },
    {
      text: 'Visiting',
      dataField: 'visitingDivision',
      sort: true,
      formatter: (col, row) => (
        <div>
          {decode(row.visitingDivision)}
          { row.visitingEmployee ? (
            <div>
              <small>
                {decode(row.visitingEmployee)}
              </small>
            </div>
          ) : null}
        </div>
      )
    },
    {
      text: 'Sign-In',
      dataField: 'createdAt',
      sort: true,
      formatter: (col, row) => (
        <div>
          <div>{Day(col).format('DD MMM YYYY / HH:mm')}</div>
          { row.editedAt ? (
            <div>
              <small style={{fontSize: '.7em'}}>
                <em>
                  Edited {Day(row.editedAt).format('DD MMM YYYY / HH:mm')}
                </em>
              </small>
            </div>
          ) : null}
        </div>
      )
    },
    {
      text: 'Sign-Out',
      dataField: 'deletedAt',
      sort: true,
      formatter: (cell, row) => (
        <div>
          {cell ? Day(cell).format('DD MMM YYYY / HH:mm') : '--'}
          <div>
            <small>
              Duration: {Day(row.createdAt).from(row.deletedAt || new Date(), true)}
            </small>
          </div>
        </div>
      )
    },
    {
      text: 'Opts',
      dataField: '',
      isDummyField: true,
      headerStyle: {
        width: '135px'
      },
      formatter: (cell, row, index) =>
        row.deletedAt ? (
          <React.Fragment>
            <button className="btn btn-warning btn-sm" title="Sign Back In" onClick={() => signInVisitor(row)}>
              <i className="fa fa-sign-in-alt fa-rotate-90" />
            </button>
            <button className="btn btn-danger btn-sm mx-2" title="Purge Record" onClick={() => deleteVisitorRecord(row)}>
              <i className="fa fa-trash" />
            </button>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <button className="btn btn-primary btn-sm" title="Print Badge" onClick={() => printVisitorBage(row)}>
              <i className="fa fa-print" />
            </button>
            <button className="btn btn-success btn-sm mx-2" title="Sign Out" onClick={() => signOutVisitor(row)}>
              <i className="fa fa-sign-out-alt" />
            </button>
            <button className="btn btn-warning btn-sm" title="Edit Record" onClick={() => editVisitorRecord(row)}>
              <i className="fa fa-edit" />
            </button>
          </React.Fragment>
        )
    }
  ]

  return (
    <div>
      <PageMask show={maskPage} />

      { showBadgeForVisitor ? (
        <BadgePrint visitor={showBadgeForVisitor} />
      ) : null }

      { editingVisitor ? (
        <Modal show={editingVisitor} size="lg">
          <div className="modal-content">
            <div className="modal-header">
              <h3>
                Editing Visitor
              </h3>
            </div>
            <div className="modal-body">
              <form>
                <div className="row">
                  <div className="col-2">
                    <label htmlFor="visitor-designation" className="form-label">
                      Guest Type
                    </label>
                    <select id="visitor-designation" required className={`form-control ${editingVisitor.designation ? 'is-valid' : 'is-invalid'}`} value={decode(editingVisitor.designation)} onChange={(e) => setEditingVisitor({ ...editingVisitor, designation: e.target.value})}>
                      <option value="">Select a Type...</option>
                      <option value="Visitor">Visitor</option>
                      <option value="Contractor">Contractor</option>
                    </select>
                  </div>
                  <div className="col-3">
                    <label htmlFor="visitor-first-name" className="form-label">
                      First Name
                    </label>
                    <input id="visitor-first-name" required type="text" className={`form-control ${editingVisitor.firstName ? 'is-valid' : 'is-invalid'}`} autoComplete="off" value={decode(editingVisitor.firstName)} onChange={(e) => setEditingVisitor({ ...editingVisitor, firstName: e.target.value})} />
                  </div>
                  <div className="col-3">
                    <label htmlFor="visitor-last-name" className="form-label">
                      Last Name
                    </label>
                    <input id="visitor-last-name" required type="text" className={`form-control ${editingVisitor.lastName ? 'is-valid' : 'is-invalid'}`} autoComplete="off" value={decode(editingVisitor.lastName)} onChange={(e) => setEditingVisitor({ ...editingVisitor, lastName: e.target.value})} />
                  </div>
                  <div className="col-4">
                    <label htmlFor="visitor-phone-number" className="form-label">
                      Phone Number
                    </label>
                    <NumberFormat
                      className={`form-control ${(editingVisitor.phone && editingVisitor.phone.length >= 10) ? 'is-valid' : 'is-invalid'}`}
                      type="tel"
                      format="(###) ###-#### x#####"
                      mask="_"
                      value={editingVisitor.phone}
                      onValueChange={(value) => setEditingVisitor({ ...editingVisitor, phone: value.value})}
                    />
                  </div>
                </div>
                <div className="row mb-2">
                  <div className="col">
                    <label htmlFor="visitor-purpose" className="form-label">
                      { editingVisitor.designation === 'Visitor' ? 'Purpose of Visit' : 'Company'}
                    </label>
                    <input id="visitor-purpose" type="text" className={`form-control ${editingVisitor.purpose ? 'is-valid' : 'is-invalid'}`} autoComplete="off" value={decode(editingVisitor.purpose)} onChange={(e) => setEditingVisitor({ ...editingVisitor, purpose: e.target.value})} />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <label htmlFor="visitor-division" className="form-label">
                      Division Visiting
                    </label>
                    <select id="visitor-division" className={`form-control ${editingVisitor.visitingDivision ? 'is-valid' : 'is-invalid'}`} value={editingVisitor.visitingDivision} onChange={changeDivision}>
                      <option value="" style={{ fontWeight: 500 }}>Select a Division...</option>
                      { divisionList.sort((a, b) => a.sortOrder > b.sortOrder ? 1 : -1).map((division, index) => (
                        <option key={index} value={division.name}>{decode(division.name)}</option>
                      ))}
                    </select>
                  </div>
                  <div className="col">
                    <label htmlFor="visiting-employee" className="form-label">
                      Specific Person Visiting <small>(optional)</small>
                    </label>
                    <select id="visitor-employee" disabled={!editingVisitor.visitingDivision} className={`form-control`} value={editingVisitor.visitingEmployee} onChange={(e) => setEditingVisitor({...editingVisitor, visitingEmployee: e.target.value})}>
                      <option value="">Select {editingVisitor.visitingDivision ? 'an Employee' : 'a Division first'}...</option>
                      { filteredEmployeeList.map((employee, index) => (
                        <option key={index} value={employee.fullName}>
                          {decode(employee.fullName)}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className={`btn btn-primary ${saveVisitorReady() ? '' : 'disabled'}`} onClick={saveEditingVisitor}>
                <i className="fa fa-fw fa-save"></i>
                Save
              </button>
              <button type="button" className="btn btn-secondary" onClick={() => setEditingVisitor(null)}>
                Cancel
              </button>
            </div>
          </div>
        </Modal>
      ) : null}

      { purgingVisitor ? (
        <Modal show={true}>
          <div className="modal-content">
            <div className="modal-body">
              <p>
                Are you sure you wish to delete <b>{decode(purgingVisitor.fullName)}</b>?
              </p>
              <p>
                <strong>Note:</strong> This cannot be undone!
              </p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary" onClick={processDelete}>
                <i className="fa fa-fw fa-sign-out-alt"></i>
                Continue
              </button>
              <button type="button" className="btn btn-secondary" onClick={cancelDelete}>
                Cancel
              </button>
            </div>
          </div>
        </Modal>
      ) : null}

      <div className="row d-print-none">
        <div className="col">
          <h3>
            All { includeDeleted ? '' : 'Current' } Visitors (<small>{visitors.length} record{visitors.length !== 1 ? 's' : ''}</small>)
          </h3>
        </div>
        <div className="col text-end">
          <CSVLink
            data={csvOutput()}
            filename={`visitor-log-visitors-${Day().format('YYYYMMDDHHmm')}.csv`}
            className="btn btn-info mx-3"
            title="Download a Comma-Separated-Values document of all the records visible below"
            >
            <i className="fa fa-fw fa-file-download"></i> Download CSV
          </CSVLink>
          <button className={`btn ${includeDeleted ? 'btn-primary' : 'btn-secondary'}`} onClick={() => { setIncludeDeleted(!includeDeleted); getVisitors(!includeDeleted) }}>
            <i className={`fa fa-fw ${includeDeleted ? 'fa-check-square' : 'fa-square'}`}></i>
            Include Signed-Out Records?
          </button>
        </div>
      </div>

      <div className="d-print-none mt-2">
        <BootstrapTable
          bootstrap4
          keyField="_id"
          data={visitors}
          columns={tableColumns}
          striped={true}
          hover={true}
          pagination={paginationFactory({ sizePerPage: 10 })}
        />
      </div>

    </div>
  )
}

export default StaffVisitorsTab
