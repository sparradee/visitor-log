import React, { useState, useEffect } from 'react'
import { decode } from 'html-entities'
import { Modal } from 'react-bootstrap/'
import PageMask from '../../components/pageMask'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from "react-bootstrap-table2-paginator";
import { CSVLink } from 'react-csv'
import Day from 'dayjs'

import API from '../../libs/api'

// TODO: add waiting spinner

function StaffEmployeeTab (props) {

  const [employees, setEmployees] = useState([])
  const [divisions, setDivisions] = useState([])
  const [showEmployeeEditor, setShowEmployeeEditor] = useState(false)
  const [editingEmployee, setEditingEmployee] = useState()
  const [saveEmployeeReady, setSaveEmployeeReady] = useState(false)
  const [deletingEmployee, setDeletingEmployee] = useState()
  const [maskPage, setMaskPage] = useState(false)

  const emptyEmployeeRecord = { firstName: '', lastName: '', title: '', division: '' }

  const getEmployees = (includeInactive = true) => {
    setMaskPage(true)
    API.employee.getAll({ includeInactive }).then((res) => {
      setEmployees(res.data)
      setMaskPage(false)
    })
  }

  const csvOutput = () => {
    const flatData = JSON.parse(JSON.stringify(employees))
    for (const entry of flatData) {
      ['_id', 'id', '__v'].forEach((i) => delete entry[i])
      entry.createdAt = Day(entry.createdAt).format('YYYY-MM-DD HH:mm:ss')
      entry.updatedAt = entry.updatedAt ? Day(entry.updatedAt).format('YYYY-MM-DD HH:mm:ss') : ''
      entry.isActive = entry.isActive ? 'Y' : 'N'
      entry.division = (entry.division || []).map((i) => i.name).join(', ')
      for (const prop in entry) {
        entry[prop] = decode(entry[prop])
      }
    }
    return flatData
  }

  useEffect(() => getEmployees(true), [])
  useEffect(() => API.division.getAll().then((res) => setDivisions(res.data)), [])
  useEffect(() => {
    setSaveEmployeeReady(editingEmployee && editingEmployee.firstName && editingEmployee.lastName && (editingEmployee.division || !divisions.length))
  }, [editingEmployee])

  const editEmployee = (record) => {
    setEditingEmployee(record || emptyEmployeeRecord)
    setShowEmployeeEditor(true)
  }

  const saveEditingEmployee = () => {
    const data = { ...editingEmployee, division: editingEmployee?.division?.map((i) => i._id) || null }

    setMaskPage(true)
    if (data._id) {
      API.employee.update(data._id, data).then((res) => {
        setShowEmployeeEditor(false)
        getEmployees(true)
      }).catch((err) => {
        console.error(err)
        setMaskPage(false)
      })
    } else {
      API.employee.create(data).then((res) => {
        setShowEmployeeEditor(false)
        getEmployees()
      }).catch((err) => {
        console.error(err)
        setMaskPage(false)
      })
    }
  }

  const toggleActive = (record) => {
    const dataToSave = Object.assign({}, record, { isActive: !record.isActive })

    setMaskPage(true)
    API.employee.update(record._id, dataToSave).then((res) => {
      getEmployees(true)
    }).catch((err) => {
      console.error(err)
      setMaskPage(false)
    })
  }

  const deleteEmployee = (record) => {
    setDeletingEmployee(record)
  }

  const processDelete = () => {
    setMaskPage(true)
    API.employee.delete(deletingEmployee._id).then((res) => {
      setDeletingEmployee(null)
      getEmployees(true)
    }).catch((err) => {
      console.error(err)
      setMaskPage(false)
    })
  }

  const cancelDelete = () => {
    setDeletingEmployee(null)
  }

  const selectDivisions = (evt) => {
    const selectedDivisions = [...evt.target.options].filter((i) => i.selected).map((i) => ({ _id: i.value }))
    setEditingEmployee({...editingEmployee, division: selectedDivisions.length ? selectedDivisions : null})
  }

  const columns = [
    {
      dataField: 'fullName',
      text: 'Name',
      sort: true,
      sortFunc: (a, b, order, dataField, rowA, rowB) => {
        if (order === 'asc') {
          return rowA.lastName > rowB.lastName ? 1 : -1
        }
        return rowA.lastName > rowB.lastName ? -1 : 1
      },
      formatter: (col) => decode(col)
    },
    {
      dataField: 'division',
      text: 'Divisions',
      sort: true,
      formatter: (cell, row) => (cell && cell.length) ? cell.sort((a, b) => a.name > b.name ? 1 : -1).map((division, index) => (
        <div key={index}>
          {decode(division.name)}
        </div>
      )) : '--'
    },
    {
      text: 'Opts',
      dataField: '',
      isDummyField: true,
      headerStyle: {
        width: '130px'
      },
      formatter: (cell, row, index) => (
        <React.Fragment>
          <button className={`btn btn-sm ${row.isActive ? 'btn-success' : 'btn-danger'}`} title={row.isActive ? 'Employee is Active' : 'Employee is Inactive'} onClick={() => toggleActive(row)}>
            <i className={`fa ${row.isActive ? 'fa-check' : 'fa-minus-circle'}`}></i>
          </button>
          <button className="btn btn-sm btn-warning mx-2" title="Edit Employee" onClick={() => editEmployee(row)}>
            <i className="fa fa-edit"></i>
          </button>
          <button className="btn btn-sm btn-danger" title="Delete Employee" onClick={() => deleteEmployee(row)}>
            <i className="fa fa-trash"></i>
          </button>
        </React.Fragment>
      )
    }
  ]

  return (
    <div>
      <PageMask show={maskPage} />

      <div className="row">
        <div className="col">
          <h3>
            Employees (<small>{employees.length} record{employees.length !== 1 ? 's' : ''}</small>)
          </h3>
        </div>
        <div className="col text-end">
          <CSVLink
            data={csvOutput()}
            filename={`visitor-log-employees-${Day().format('YYYYMMDDHHmm')}.csv`}
            className="btn btn-info mx-3"
            title="Download a Comma-Separated-Values document of all the records visible below"
            >
            <i className="fa fa-fw fa-file-download"></i> Download CSV
          </CSVLink>
          <button className="btn btn-primary" onClick={() => editEmployee()}>
            <i className="fa fa-fw fa-user-plus"></i>
            Add New Employee
          </button>
        </div>
      </div>

      { editingEmployee ? (
        <Modal show={showEmployeeEditor}>
          <div className="modal-content">
            <div className="modal-header">
              <h3>
                {editingEmployee._id ? 'Editing' : 'Creating'} Employee
              </h3>
            </div>
            <div className="modal-body">
              <form>
                <div className="row">
                  <div className="col-3">
                    <label htmlFor="honorific" className="form-label">
                      Honorific
                    </label>
                    <select className="form-control" value={decode(editingEmployee.honorific)} onChange={(e) => setEditingEmployee({...editingEmployee, honorific: e.target.value})}>
                      <option value="">Pick One...</option>
                      { ['Mr.', 'Ms.', 'Mrs.', 'Dr.', 'Mx'].map((value, index) => (
                        <option key={index} value={value}>{value}</option>
                      ))}
                    </select>
                  </div>
                  <div className="col-9">
                    <label htmlFor="firstName" className="form-label">
                      First Name
                    </label>
                    <input type="text" required className={`form-control ${editingEmployee.firstName ? 'is-valid' : 'is-invalid'}`} id="firstName" value={decode(editingEmployee.firstName)} onChange={(e) => setEditingEmployee({...editingEmployee, firstName: e.target.value})} />
                  </div>
                </div>

                <label htmlFor="lastName" className="form-label">
                  Last Name
                </label>
                <input type="text" required className={`form-control ${editingEmployee.lastName ? 'is-valid' : 'is-invalid'}`} id="lastName" value={decode(editingEmployee.lastName)} onChange={(e) => setEditingEmployee({...editingEmployee, lastName: e.target.value})} />

                <label htmlFor="title" className="form-label">
                  Title
                </label>
                <input type="text" className={`form-control`} id="title" value={decode(editingEmployee.title)} onChange={(e) => setEditingEmployee({...editingEmployee, title: e.target.value})} />

                <label htmlFor="division" className="form-label">
                  Divisions
                </label>
                <select id="visitor-visiting" multiple={true} required className={`form-control ${editingEmployee?.division?.length ? 'is-valid' : 'is-invalid'}`} onChange={selectDivisions}>
                  { divisions.map((division, index) => (
                    <option key={index} value={division._id} selected={editingEmployee?.division?.length ? !!editingEmployee.division.find((i) => i._id === division._id) : false}>
                      {decode(division.name)}
                    </option>
                  ))}
                </select>
                { divisions.length ? (
                  <small className="text-muted">
                    <i className="fa fa-fw fa-info-circle"></i>
                    Use Ctrl + Mouse-Click to select/deselect multiple entries.
                  </small>
                ) : (
                  <small className="text-muted">
                    <i className="fa fa-fw fa-exclamation-triangle"></i>
                    There are currently no Divisions in the database.
                  </small>
                )}

                <div className="form-check mt-2">
                  <label htmlFor="isActive" className="form-check-label">
                    Employee is active
                  </label>
                  <input id="isActive" type="checkbox" className="form-check-input" checked={editingEmployee.isActive} onChange={(e) => setEditingEmployee({...editingEmployee, isActive: e.target.checked })} />
                </div>

              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className={`btn btn-primary ${saveEmployeeReady ? '' : 'disabled'}`} onClick={saveEditingEmployee}>
                <i className="fa fa-fw fa-save"></i>
                Save
              </button>
              <button type="button" className="btn btn-secondary" onClick={() => setShowEmployeeEditor(false)}>
                Cancel
              </button>
            </div>
          </div>
        </Modal>
      ) : null}

      { deletingEmployee ? (
        <Modal show={true}>
          <div className="modal-content">
            <div className="modal-body">
              <p>
                Are you sure you wish to delete <b>{decode(deletingEmployee.fullName)}</b>?
              </p>
              <p>
                <strong>Note:</strong> This cannot be undone!
              </p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary" onClick={processDelete}>
                <i className="fa fa-fw fa-sign-out-alt"></i>
                Continue
              </button>
              <button type="button" className="btn btn-secondary" onClick={cancelDelete}>
                Cancel
              </button>
            </div>
          </div>
        </Modal>
      ) : null}

      <div className="mt-2">
        <BootstrapTable
          bootstrap4
          keyField="_id"
          data={employees}
          columns={columns}
          rowClasses={(row, rowIndex) => !row.isActive ? 'table-secondary' : ''}
          pagination={employees.length > 25 ? paginationFactory({ sizePerPage: 25 }) : null}
        />
      </div>

    </div>
  )
}

export default StaffEmployeeTab
