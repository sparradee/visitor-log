import React, { useState, useEffect } from 'react'
import { decode } from 'html-entities'
import { Modal } from 'react-bootstrap/'
import PageMask from '../../components/pageMask'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from "react-bootstrap-table2-paginator";
import { CSVLink } from 'react-csv'
import Day from 'dayjs'
import API from '../../libs/api'

// TODO: add waiting spinner

function StaffDivisionTab (props) {

  const [divisions, setDivisions] = useState([])
  const [showDivisionEditor, setShowDivisionEditor] = useState(false)
  const [editingDivision, setEditingDivision] = useState()
  const [saveDivisionReady, setSaveDivisionReady] = useState(false)
  const [deletingDivision, setDeletingDivision] = useState()
  const [maskPage, setMaskPage] = useState(false)

  const getDivisions = () => {
    setMaskPage(true)
    API.division.getAll().then((res) => {
      setDivisions(res.data)
      setMaskPage(false)
    })
  }

  const csvOutput = () => {
    const flatData = JSON.parse(JSON.stringify(divisions))
    for (const entry of flatData) {
      ['_id', 'id', '__v'].forEach((i) => delete entry[i])
      entry.createdAt = Day(entry.createdAt).format('YYYY-MM-DD HH:mm:ss')
      entry.updatedAt = entry.updatedAt ? Day(entry.updatedAt).format('YYYY-MM-DD HH:mm:ss') : ''
      for (const prop in entry) {
        entry[prop] = decode(entry[prop])
      }
    }
    return flatData
  }

  const nameExists = () => !!divisions.find((item) => item.name.toLowerCase() === editingDivision?.name?.toLowerCase() && item._id !== editingDivision?._id)

  useEffect(() => getDivisions(), [])
  useEffect(() => API.division.getAll().then((res) => setDivisions(res.data)), [])
  useEffect(() => {
    setSaveDivisionReady(editingDivision && editingDivision.name && !nameExists())
  }, [editingDivision])

  const editDivision = (record) => {
    setEditingDivision(record || { name: '', sortOrder: divisions.length + 1 })
    setShowDivisionEditor(true)
  }

  const saveEditingDivision = () => {
    setMaskPage(true)
    if (editingDivision._id) {
      API.division.update(editingDivision._id, editingDivision).then((res) => {
        setShowDivisionEditor(false)
        getDivisions()
      }).catch((err) => {
        console.error(err)
        setMaskPage(false)
      })
    } else {
      API.division.create(editingDivision).then((res) => {
        setShowDivisionEditor(false)
        getDivisions()
      }).catch((err) => {
        console.error(err)
        setMaskPage(false)
      })
    }
  }

  const deleteDivision = (record) => {
    setDeletingDivision(record)
  }

  const processDelete = () => {
    setMaskPage(true)
    API.division.delete(deletingDivision._id).then((res) => {
      setDeletingDivision(null)
      normalizeSortOrder()
      getDivisions()
    }).catch((err) => {
      console.error(err)
      setMaskPage(false)
    })
  }

  const cancelDelete = () => {
    setDeletingDivision(null)
  }

  const moveUp = (row) => {
    if (row?.sortOrder > 1) {
      const desiredPosition = row.sortOrder - 1
      const newDivisions = JSON.parse(JSON.stringify(divisions))
      for (const otherDivision of newDivisions) {
        if (otherDivision._id.toString() === row._id.toString()) {
          otherDivision.sortOrder--
        } else if (otherDivision.sortOrder === desiredPosition) {
          otherDivision.sortOrder++
        }
      }
      normalizeSortOrder(newDivisions)
    }
  }

  const moveDown = (row) => {
    if (row?.sortOrder < divisions.length) {
      const desiredPosition = row.sortOrder + 1
      const newDivisions = JSON.parse(JSON.stringify(divisions))
      for (const otherDivision of newDivisions) {
        if (otherDivision._id.toString() === row._id.toString()) {
          otherDivision.sortOrder++
        } else if (otherDivision.sortOrder === desiredPosition) {
          otherDivision.sortOrder--
        }
      }
      normalizeSortOrder(newDivisions)
    }
  }

  /**
   * Re-number the sortOrder of all the provided divisions, to ensure they are properly numbered.
   *
   * @param {[object]} [list] (optional) Array of division items
   */
  const normalizeSortOrder = async (list) => {
    const normalizedList = []
    let index = 0
    if (!list) {
      list = [...divisions]
    }
    list.sort((a, b) => a.sortOrder > b.sortOrder ? 1 : -1).forEach((item) => {
      normalizedList.push(Object.assign({}, item, { sortOrder: ++index }))
    })

    const modifiedItems = normalizedList.filter((i) => divisions.find((o) => o._id.toString() === i._id.toString()).sortOrder !== i.sortOrder)

    await Promise.all(modifiedItems.map((item) => API.division.update(item._id, item).then((res) => {}).catch((err) => {console.error(err)})))

    setDivisions(normalizedList)
  }

  const tableColumns = [
    {
      dataField: 'sortOrder',
      sort: true,
      hidden: true
    },
    {
      text: 'Name',
      dataField: 'name',
      sort: false,
      formatter: (col) => decode(col)
    },
    {
      text: 'Options',
      dataField: '',
      isDummyField: true,
      headerStyle: {
        width: '175px'
      },
      formatter: (cell, row, index) => (
        <React.Fragment>
          <button className="btn btn-primary btn-sm mx-1" title="Move Up" disabled={index === 0} onClick={() => moveUp(row)}>
            <i className="fa fa-arrow-up"></i>
          </button>
          <button className="btn btn-primary btn-sm mx-1" title="Move Down" disabled={index === divisions.length - 1} onClick={() => moveDown(row)}>
            <i className="fa fa-arrow-down"></i>
          </button>
          <button className="btn btn-warning btn-sm mx-1" title="Edit Division" onClick={() => editDivision(row)}>
            <i className="fa fa-edit"></i>
          </button>
          <button className="btn btn-danger btn-sm mx-1" title="Delete Division" onClick={() => deleteDivision(row)}>
            <i className="fa fa-trash"></i>
          </button>
        </React.Fragment>
      )
    }
  ]

  return (
    <div>
      <PageMask show={maskPage} />

      <div className="row">
        <div className="col">
          <h3>
            Divisions (<small>{divisions.length} record{divisions.length !== 1 ? 's' : ''}</small>)
          </h3>
        </div>
        <div className="col text-end">
          <CSVLink
            data={csvOutput()}
            filename={`visitor-log-divisions-${Day().format('YYYYMMDDHHmm')}.csv`}
            className="btn btn-info mx-3"
            title="Download a Comma-Separated-Values document of all the records visible below"
            >
            <i className="fa fa-fw fa-file-download"></i> Download CSV
          </CSVLink>
          <button className="btn btn-primary" onClick={() => editDivision()}>
            <i className="fa fa-fw fa-plus"></i>
            Add New Division
          </button>
        </div>
      </div>

      { editingDivision ? (
        <Modal show={showDivisionEditor}>
          <div className="modal-content">
            <div className="modal-header">
              <h3>
                {editingDivision._id ? 'Editing' : 'Creating'} Division
              </h3>
            </div>
            <div className="modal-body">
              <form>
                <label htmlFor="name" className="form-label">
                  Name
                </label>
                <input type="text" required className={`form-control ${editingDivision.name && !nameExists() ? 'is-valid' : 'is-invalid'}`} id="name" value={decode(editingDivision.name)} onChange={(e) => setEditingDivision({...editingDivision, name: e.target.value})} />
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className={`btn btn-primary ${saveDivisionReady ? '' : 'disabled'}`} onClick={saveEditingDivision}>
                <i className="fa fa-fw fa-save"></i>
                Save
              </button>
              <button type="button" className="btn btn-secondary" onClick={() => setShowDivisionEditor(false)}>
                Cancel
              </button>
            </div>
          </div>
        </Modal>
      ) : null}

      { deletingDivision ? (
        <Modal show={true}>
          <div className="modal-content">
            <div className="modal-body">
              <p>
                Are you sure you wish to delete <b>{decode(deletingDivision.name)}</b>?
              </p>
              <p>
                <strong>Note:</strong> This cannot be undone!
              </p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary" onClick={processDelete}>
                <i className="fa fa-fw fa-sign-out-alt"></i>
                Continue
              </button>
              <button type="button" className="btn btn-secondary" onClick={cancelDelete}>
                Cancel
              </button>
            </div>
          </div>
        </Modal>
      ) : null}

      <div className="d-print-none mt-2">
        <BootstrapTable
          bootstrap4
          keyField="_id"
          data={divisions}
          columns={tableColumns}
          striped={true}
          hover={true}
          pagination={paginationFactory({ sizePerPage: 25 })}
          defaultSorted={[{ dataField: 'sortOrder', order: 'asc' }]}
        />
      </div>

    </div>
  )
}

export default StaffDivisionTab
