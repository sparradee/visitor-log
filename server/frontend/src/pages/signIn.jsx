import React, { useRef, useState, useEffect } from 'react'
import { decode } from 'html-entities'
import { Modal } from 'react-bootstrap'
import { Link, useNavigate } from 'react-router-dom'
import NumberFormat from 'react-number-format'
import PageMask from '../components/pageMask'
import API from '../libs/api'
import BadgePrint from '../components/badgePrint'
import BootstrapTable from 'react-bootstrap-table-next'
import Day from 'dayjs'
import Webcam from 'react-webcam'
import CONFIG from '../config.json'

function SignInPage () {

  const defaultVisitorData = {
    _id: null,
    designation: '',
    lastName: '',
    firstName: '',
    phoneNumber: '',
    purpose: '',
    visitingDivision: '',
    visitingEmployee: '',
    date: null,
    photo: null,
    _lockForm: false,
    _ready: false
  }

  const navigateTo = useNavigate()
  const webcamRef = useRef(null)

  const [state, setState] = useState(defaultVisitorData)
  const [maskPage, setMaskPage] = useState(false)
  const [divisionList, setDivisionList] = useState([])
  const [employeeList, setEmployeeList] = useState([])
  const [filteredEmployeeList, setFilteredEmployeeList] = useState([])
  const [showBadgeForVisitor, setVisitorBadge] = useState(null)
  const [confirmDiscard, setConfirmDiscard] = useState(false)
  const [showPhotoForm, setShowPhotoForm] = useState(false)

  const capture = () => {
    setState({...state, photo: webcamRef.current.getScreenshot()})
  }

  const resetCapture = () => {
    setState({...state, photo: null})
  }

  useEffect(() => {
    API.division
      .getAll()
      .then((res) => setDivisionList(res.data.sort((a, b) => a.name > b.name ? 1 : -1)))

    API.employee
      .getAll({ includeInactive: false })
      .then((res) => setEmployeeList(res.data.sort((a, b) => a.lastName > b.lastName ? 1 : -1)))

  }, [])

  const formReady = () => {
    return (
      state.designation &&
      state.lastName &&
      state.firstName &&
      state.purpose &&
      state.visitingDivision &&
      state.phoneNumber &&
      state.phoneNumber.length >= 10 &&
      ( showPhotoForm ? (
        state.photo
      ) : (
        true
      ))
    )
  }

  const startPhotoForm = () => {
    setState({ ...state,  _lockForm: true })
    setShowPhotoForm(true)
  }

  const submitForm = async () => {
    const dataToSubmit = {
      firstName: state.firstName,
      lastName: state.lastName,
      phone: state.phoneNumber || '',
      designation: state.designation,
      purpose: state.purpose,
      visitingDivision: state.visitingDivision,
      visitingEmployee: state.visitingEmployee,
      date: new Date(),
      photo: state.photo
    }

    setMaskPage(true)
    if (state._id) {
      await API.visitor.update(state._id, dataToSubmit)
        .then((res) => {
          setState({ ...state,  _lockForm: true, _ready: true })
        })
        .catch((err) => {
          console.error(err)
        })
    } else {
      await API.visitor.create(dataToSubmit)
        .then((res) => {
          setState({ ...state,  _lockForm: true, _ready: true, _id: res.data._id, date: dataToSubmit.date })
        })
        .catch((err) => {
          console.error(err)
        })
    }
    setMaskPage(false)
  }

  const clearForm = () => {
    setState(defaultVisitorData)
  }

  const editForm = () => {
    setState({ ...state,  _lockForm: false, _ready: false })
  }

  const printBadge = () => {
    setVisitorBadge(state)
    setTimeout(() => {
      window.print()
      navigateTo('/home')
    }, 150)
  }

  const isVisitor = state.designation === 'Visitor'

  const enforcePhoneFormat = (evt) => {
    const value = evt.target.value.replace(/([^0-9])/g, '').substr(0,10)
    const formattedValue = value.length <= 7 ? value.replace(/^([0-9]{1,3})([0-9]{4})$/, '$1-$2') : value.replace(/^([0-9]{1,3})([0-9]{3})([0-9]{4})$/, '($1) $2-$3')
    setState({ ...state, phoneNumber: formattedValue})
  }

  const changeDivision = (evt) => {
    const divisionName = evt.target.value
    setState({ ...state, visitingDivision: divisionName, visitingEmployee: '' })
    if (employeeList.length) {
      setFilteredEmployeeList(employeeList.filter((employee) => employee?.division?.length ? !!employee.division.find((div) => div.name === divisionName) : false))
    }
  }

  const discardEntry = () => {
    setConfirmDiscard(true)
  }

  const cancelDiscard = () => {
    setConfirmDiscard(false)
  }

  const processDiscard = () => {
    setMaskPage(true)
    API.visitor
      .delete(state._id)
      .then((res) => {
        setMaskPage(false)
        setConfirmDiscard(false)
        navigateTo('/home')
      })
      .catch((err) => {
        setMaskPage(false)
        console.error(err)
      })
  }

  const tableColumns = [
    {
      text: 'Photo',
      dataField: 'photo',
      headerStyle: {
        width: '100px'
      },
      formatter: (col) => <img src={col} className="img img-fluid" />
    },
    {
      text: 'Designation',
      dataField: 'designation',
      headerStyle: {
        width: '135px'
      },
      formatter: (col) => decode(col)
    },
    {
      text: 'Name',
      dataField: 'lastName',
      formatter: (cell, row, index) => (<div>{decode(row.firstName)} {decode(row.lastName)}</div>)
    },
    {
      text: 'Phone',
      dataField: 'phoneNumber'
    },
    {
      text: 'Purpose / Company',
      dataField: 'purpose',
      formatter: (col) => decode(col)
    },
    {
      text: 'Visiting',
      dataField: 'visitingDivision',
      formatter: (cell, row, index) => (
        <div>
          {decode(row.visitingDivision)}
          { row.visitingEmployee ? (
            <div>
              {decode(row.visitingEmployee)}
            </div>
          ) : null}
        </div>
      )
    },
    {
      text: 'Sign-In',
      dataField: 'createdAt',
      formatter: (cell, row, index) => (<div>{Day(cell).format('DD MMM YYYY / HH:mm')}</div>)
    }
  ]

  return (
    <div>
      <PageMask show={maskPage} />

      { showBadgeForVisitor ? (
        <BadgePrint visitor={showBadgeForVisitor} done={() => console.log('done')} />
      ) : null }

      <form autoComplete="off" className="d-print-none">
        <div className="row mb-2">
          <div className="col">
            <label htmlFor="visitor-designation" className="form-label">
              Guest Type
            </label>
            <select id="visitor-designation" required disabled={state._lockForm} className={`form-control ${state.designation ? 'is-valid' : 'is-invalid'}`} value={decode(state.designation)} onChange={(e) => setState({ ...state, designation: e.target.value})}>
              <option value="">Select a Type...</option>
              <option value="Visitor">Visitor</option>
              <option value="Contractor">Contractor</option>
            </select>
          </div>
          <div className="col">
            <label htmlFor="visitor-first-name" className="form-label">
              First Name
            </label>
            <input id="visitor-first-name" required type="text" disabled={state._lockForm} className={`form-control ${state.firstName ? 'is-valid' : 'is-invalid'}`} autoComplete="off" value={decode(state.firstName)} onChange={(e) => setState({ ...state, firstName: e.target.value})} />
          </div>
          <div className="col">
            <label htmlFor="visitor-last-name" className="form-label">
              Last Name
            </label>
            <input id="visitor-last-name" required type="text" disabled={state._lockForm} className={`form-control ${state.lastName ? 'is-valid' : 'is-invalid'}`} autoComplete="off" value={decode(state.lastName)} onChange={(e) => setState({ ...state, lastName: e.target.value})} />
          </div>
          <div className="col">
            <label htmlFor="visitor-phone-number" className="form-label">
              Phone Number
            </label>
            <NumberFormat
              disabled={state._lockForm}
              className={`form-control ${(state.phoneNumber && state.phoneNumber.length >= 10) ? 'is-valid' : 'is-invalid'}`}
              type="tel"
              format="(###) ###-#### x#####"
              mask="_"
              value={state.phoneNumber}
              onValueChange={(value) => setState({ ...state, phoneNumber: value.value })}
            />
          </div>
        </div>
        <div className="row mb-2">
          <div className="col">
            <label htmlFor="visitor-purpose" className="form-label">
              { isVisitor ? 'Purpose of Visit' : 'Company'}
            </label>
            <input id="visitor-purpose" type="text" disabled={state._lockForm} className={`form-control ${state.purpose ? 'is-valid' : 'is-invalid'}`} autoComplete="off" value={decode(state.purpose)} onChange={(e) => setState({ ...state, purpose: e.target.value})} />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <label htmlFor="visitor-division" className="form-label">
              Division Visiting
            </label>
            <select id="visitor-division" disabled={state._lockForm} className={`form-control ${state.visitingDivision ? 'is-valid' : 'is-invalid'}`} value={state.visitingDivision} onChange={changeDivision}>
              <option value="" style={{ fontWeight: 500 }}>Select a Division...</option>
              { divisionList.sort((a, b) => a.sortOrder > b.sortOrder ? 1 : -1).map((division, index) => (
                <option key={index} value={division.name}>{decode(division.name)}</option>
              ))}
            </select>
          </div>
          <div className="col">
            <label htmlFor="visiting-employee" className="form-label">
              Specific Person Visiting <small>(optional)</small>
            </label>
            <select id="visitor-employee" disabled={state._lockForm || !state.visitingDivision} className={`form-control`} value={state.visitingEmployee} onChange={(e) => setState({ ...state, visitingEmployee: e.target.value })}>
              <option value="">Select {state.visitingDivision ? 'an Employee' : 'a Division first'}...</option>
              { filteredEmployeeList.map((employee, index) => (
                <option key={index} value={employee.fullNameNoTitle}>
                  {decode(employee.fullNameNoTitle)}
                </option>
              ))}
            </select>
          </div>
          <div className="col-5">
            <div className="row pt-4">
              <div className="col d-grid">
                <div className={`btn btn-primary ${formReady() && !state._lockForm ? '' : 'disabled'}`} onClick={(CONFIG.WEBCAM && !state._id) ? startPhotoForm : submitForm}>
                  <i className="fa fa-fw fa-save"></i>
                  Submit {state._id ? 'Edits' : ''}
                </div>
              </div>
              { !state._id ? (
                <>
                  <div className="col d-grid">
                    <div className={`btn btn-primary ${state._lockForm ? 'disabled' : ''}`} onClick={clearForm}>
                      <i className="fa fa-fw fa-times"></i>
                      Clear
                    </div>
                  </div>
                  <div className="col d-grid">
                    <Link to="/home" className={`btn btn-primary ${state._lockForm ? 'disabled' : ''}`}>
                      Cancel
                    </Link>
                  </div>
                </>
              ) : null}
            </div>
          </div>
        </div>
      </form>

      { (showPhotoForm && !state._ready) ? (
        <div className="col text-center mt-3">
          { !state.photo ? (
            <>
              <div>
                <div className="webcam-photo-area mx-auto shadow rounded">
                  <Webcam height="100%" audio={false} ref={webcamRef} screenshotFormat="image/png" />
                  <div className="webcam-overlay-left"></div>
                  <div className="webcam-overlay-right"></div>
                </div>
              </div>
              <div>
                <button type="button" className={`btn btn-danger mt-2 ${(state._ready) ? 'disabled' : ''}`} onClick={capture}>
                  <i className="fa fa-fw fa-camera"></i>
                  Take Photo
                </button>
              </div>
            </>
          ) : (
            <>
              <div>
                <div className="webcam-photo-area mx-auto shadow rounded">
                  <img src={state.photo} />
                  <div className="webcam-overlay-left"></div>
                  <div className="webcam-overlay-right"></div>
                </div>
              </div>
              <div className="mt-2">
                <button type="button" className="btn btn-primary mx-1" onClick={resetCapture}>
                  <i className="fa fa-fw fa-times"></i>
                  Re-Take Photo
                </button>
                { !state._id ? (
                  <button type="button" className="btn btn-primary mx-1" onClick={submitForm}>
                    <i className="fa fa-fw fa-save"></i>
                    Submit
                  </button>
                ) : null}
              </div>
            </>
          )}
          { !state._id ? (
            <div className="row mt-2">
              <div className="col-6 offset-3">
                <div className="alert alert-info p-2">
                  <b>Note:</b> You may make changes to the form after you submit your photo.
                </div>
              </div>
            </div>
          ) : null }
        </div>
      ) : null}

      { state._ready ? (
        <div className="d-print-none mt-2">
          <BootstrapTable
            bootstrap4
            keyField="_id"
            data={[state]}
            columns={tableColumns}
          />
        </div>
      ) : null}

      { confirmDiscard ? (
        <div className="d-print-none">
          <Modal show={true}>
            <div className="modal-content">
              <div className="modal-body">
                <p>
                  Are you sure you wish to discard this entry?
                </p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" onClick={processDiscard}>
                  <i className="fa fa-fw fa-trash"></i>
                  Discard
                </button>
                <button type="button" className="btn btn-secondary" onClick={cancelDiscard}>
                  Cancel
                </button>
              </div>
            </div>
          </Modal>
        </div>
      ) : null}

      { state._ready ? (
        <div className="card d-print-none">
          <div className="card-body">
            <div className="text-center fs-3">
              <p>
                The above entry has been recorded to the guest log.
              </p>
              <p>
                If any corrections need to be made, press the <b>Edit</b> button below.
              </p>
              <p>
                If the data is correct, press <b>Print</b> to create your visitor badge.
              </p>
            </div>

            <div className="row">
              <div className="col text-center">
                <div className="btn btn-primary btn-lg fs-3" onClick={editForm}>
                  <i className="fa fa-fw fa-edit"></i>
                  Edit
                </div>
              </div>
              <div className="col text-center">
                <button type="button" className="btn btn-warning btn-lg fs-3" onClick={discardEntry}>
                  <i className="fa fa-fw fa-trash"></i>
                  Discard
                </button>
              </div>
              <div className="col text-center">
                <button type="button" target="_blank" className="btn btn-primary btn-lg fs-3" onClick={printBadge}>
                  <i className="fa fa-fw fa-print"></i>
                  Print
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  )
}

export default SignInPage
