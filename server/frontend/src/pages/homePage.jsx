import React from 'react'
import { Link } from 'react-router-dom'

function HomePage () {
  return (
    <div>
      <div className="row">
        <div className="col text-center my-2">
          <Link to="/signin" className="btn btn-primary p-4 fs-1" style={{ width: 300 }} >
            <i className="fa fa-fw fa-sign-in-alt fa-rotate-90"></i>
            Sign-In
          </Link>
        </div>
      </div>
      <div className="row">
        <div className="col text-center my-2">
          <Link to="/signout" className="btn btn-primary p-4 fs-1" style={{ width: 300 }} >
            <i className="fa fa-fw fa-sign-out-alt"></i>
            Sign-Out
          </Link>
        </div>
      </div>
    </div>
  )
}

export default HomePage
