import React, { useState, useEffect } from 'react'
import { decode } from 'html-entities'
import { Modal, Tooltip, OverlayTrigger } from 'react-bootstrap'
import { Link, useNavigate  } from 'react-router-dom'
import PageMask from '../components/pageMask'
import API from '../libs/api'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from "react-bootstrap-table2-paginator";
import Day from 'dayjs'
import CONFIG from '../config.json'

const fetchVisitors = (setVisitors, setMaskPage) => {
  if (setMaskPage) {
    setMaskPage(true)
  }
  API.visitor.getAll().then((res) => {
    setVisitors(res.data)
    if (setMaskPage) {
      setMaskPage(false)
    }
  })
}

function SignOutPage () {
  const [visitors, setVisitors] = useState([])
  const [showModal, setShowModal] = useState(false)
  const [selectedVisitor, setSelectedVisitor] = useState({})
  const [maskPage, setMaskPage] = useState(false)
  const [csvData, setCsvData] = useState([])

  const navigateTo = useNavigate()

  useEffect(() => fetchVisitors(setVisitors, setMaskPage), [])

  const selectRow = (selectedVisitor) => {
    for (const visitor of visitors) {
      visitor._selected = selectedVisitor._id === visitor._id && !selectedVisitor._selected
    }
    setSelectedVisitor(visitors.find((visitor) => !!visitor._selected) || {})
  }

  const oneIsSelected = () => !!visitors.some((visitor) => !!visitor._selected)

  const signOutSelected = () => {
    setShowModal(true)
  }

  const processSignOut = () => {
    setMaskPage(true)
    API.visitor.update(selectedVisitor._id, { deletedAt: new Date() }).then(() => {
      navigateTo('/home')
    }).catch((err) => {
      console.error(err)
    })
  }

  const cancelSignOut = () => {
    setShowModal(false)
  }

  const tableColumns = [
    CONFIG.WEBCAM ? {
      text: 'Photo',
      dataField: 'photo',
      headerStyle: {
        width: '100px'
      },
      formatter: (col) => !!col ? (
        <OverlayTrigger placement="top" delay={{ show: 250, hide: 150 }} overlay={(
            <Tooltip>
              <img src={col} className="img img-fluid" />
            </Tooltip>
          )}
          >
          <img src={col} className="img img-fluid" />
        </OverlayTrigger>
      ) : (
        <div className="text-center fa-stack fa-2x" style={{ opacity: 0.33 }}>
          <i className="fa fa-camera fa-stack-1x"></i>
          <i className="fa fa-ban fa-stack-2x" style={{ color: 'Tomato' }}></i>
        </div>
      )
    } : { dataField: 'photo', hidden: true },
    {
      text: 'Designation',
      dataField: 'designation',
      sort: true,
      headerStyle: {
        width: '135px'
      },
      formatter: (col) => decode(col)
    },
    {
      text: 'Name',
      dataField: 'fullName',
      sort: true,
      formatter: (col) => decode(col)
    },
    {
      text: 'Phone',
      dataField: 'maskedPhone',
      sort: true
    },
    {
      text: 'Purpose / Company',
      dataField: 'purpose',
      sort: true,
      formatter: (col) => decode(col)
    },
    {
      text: 'Visiting',
      dataField: 'visitingDivision',
      sort: true,
      formatter: (cell, row) => (
        <div>
          {decode(row.visitingDivision)}
          { row.visitingEmployee ? (
            <div>
              {decode(row.visitingEmployee)}
            </div>
          ) : null}
        </div>
      )
    },
    {
      text: 'Sign-In',
      dataField: 'createdAt',
      sort: true,
      formatter: (cell, row, index) => (<div>{Day(cell).format('DD MMM YYYY / HH:mm')}</div>)
    },
    {
      text: 'Time on Site',
      dataField: '_id',
      formatter: (cell, row, index) => (<div>{Day(row.createdAt).from(row.deletedAt || new Date(), true)}</div>)
    }
  ]

  return (
    <div>
      <PageMask show={maskPage} />

      <div className="row">
        <div className="col">
          <h3>
            Currently Signed-In Visitors
          </h3>
        </div>
        <div className="col text-end">
          <button type="button" className="btn btn-primary btn-sm" onClick={() => fetchVisitors(setVisitors, setMaskPage)}>
            <i className="fa fa-fw fa-sync"></i>
            Refresh
          </button>
        </div>
      </div>

      <Modal show={showModal}>
        <div className="modal-content">
          <div className="modal-body">
            <p>
              Are you sure you wish to sign out <b>{selectedVisitor.designation}</b> "<b>{selectedVisitor.fullName}</b>"?
            </p>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-primary" onClick={processSignOut}>
              <i className="fa fa-fw fa-sign-out-alt"></i>
              Continue
            </button>
            <button type="button" className="btn btn-secondary" onClick={cancelSignOut}>
              Cancel
            </button>
          </div>
        </div>
      </Modal>

      <div className="d-print-none mt-2">
        <BootstrapTable
          bootstrap4
          keyField="_id"
          data={visitors}
          columns={tableColumns}
          striped={true}
          hover={true}
          rowClasses={(row, rowIndex) => row._id === selectedVisitor._id ? 'table-primary' : null}
          sort={{ dataField: 'createdAt', order: 'asc' }}
          selectRow={{
            mode: 'radio',
            hideSelectColumn: true,
            clickToSelect: true,
            onSelect: (row, isSelect, index, e) => selectRow(row)
          }}
          pagination={visitors.length > 10 ? paginationFactory({ sizePerPage: 10 }) : null}
        />
      </div>

      <div className="card">
        <div className="card-body">
          <div className="text-center fs-3">
            <p>
              Please select your information from the list above, then click <b>Sign-Out</b>.
            </p>
          </div>
          <div className="row">
            <div className="col-2">&nbsp;</div>
            <div className="col d-grid">
              <div className={`mx-4 btn btn-primary btn-lg fs-3 ${oneIsSelected() ? '' : 'disabled'}`} onClick={signOutSelected}>
                <i className="fa fa-fw fa-sign-out-alt"></i>
                Sign-Out
              </div>
            </div>
            <div className="col d-grid">
              <Link to="/home" className="mx-4 btn btn-primary btn-lg fs-3">
                Cancel
              </Link>
            </div>
            <div className="col-2">&nbsp;</div>
          </div>
        </div>
      </div>

    </div>
  )
}

export default SignOutPage
