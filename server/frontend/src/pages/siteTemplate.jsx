import React from 'react'
import { Link, Outlet, useMatch, useResolvedPath } from 'react-router-dom'
import { version } from '../../package.json'
import CurrentTime from '../libs/currentTime'

function Template () {
  const onStaffPage = useMatch({ path: useResolvedPath('/staff').pathname, end: true })
  const onHomePage = useMatch({ path: useResolvedPath('/home').pathname, end: true })

  return (
    <div className="container">
      <div className="card mt-2">
        <div className="card-heaer d-print-none">
          <div className="card-header">
            <div className="row">
              <div className="col text-center">
                <h4 className="mb-0">
                  Sandhills School Guest Management System
                </h4>
                <div>
                  <i className="fa fa-fw fa-clock"></i>
                  <b>Current Time:</b> <CurrentTime />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="card-body position-relative p-4 bg-with-logo background-logo">
          <Outlet />
        </div>
        <div className="card-footer d-print-none">
          <div className="row">
            <div className="col">
              &nbsp;
            </div>
            <div className="col text-center">
              { onStaffPage ? (
                <Link to="/home" className="btn btn-secondary">
                  <i className="fa fa-fw fa-home"></i>
                  Home
                </Link>
              ) : onHomePage ? (
                <Link to="/staff" className="btn btn-secondary">
                  <i className="fa fa-fw fa-user-tie"></i>
                  Admin
                </Link>
              ) : null}
              </div>
            <div className="col mt-2 text-end">
              <small className="text-muted">
                Version {version}
              </small>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Template
