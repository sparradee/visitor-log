import React, { useState } from 'react'
import StaffVisitorsTab from './tabs/staffVisitorsTab'
import StaffEmployeeTab from './tabs/staffEmployeeTab'
import StaffDivisionTab from './tabs/staffDivisionTab'

function StaffPage () {

  const [activeTab, setActiveTab] = useState('visitors')

  return (
    <div>
      <ul className="nav nav-tabs nav-fill d-print-none">
        <li className="nav-item">
          <button type="button" className={`nav-link ${activeTab === 'visitors' ? 'active' : ''}`} onClick={() => setActiveTab('visitors')}>
            <i className="fa fa-fw fa-list"></i>
            Guests
          </button>
        </li>
        <li className="nav-item text-end">
          <button type="button" className={`nav-link ${activeTab === 'employees' ? 'active' : ''}`} onClick={() => setActiveTab('employees')}>
            <i className="fa fa-fw fa-users"></i>
            Employees
          </button>
        </li>
        <li className="nav-item">
          <button type="button" className={`nav-link ${activeTab === 'divisions' ? 'active' : ''}`} onClick={() => setActiveTab('divisions')}>
            <i className="fa fa-fw fa-list-alt"></i>
            Divisions
          </button>
        </li>
      </ul>

      <div className="tab-content pt-2">
        { activeTab === 'visitors' ? <StaffVisitorsTab /> : '' }
        { activeTab === 'employees' ? <StaffEmployeeTab /> : '' }
        { activeTab === 'divisions' ? <StaffDivisionTab /> : '' }
      </div>
    </div>
  )
}

export default StaffPage
