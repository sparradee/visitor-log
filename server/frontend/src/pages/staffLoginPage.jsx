import React, { useState } from 'react'
import { Modal } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import StaffPage from './staffPage'
import CONFIG from '../config.json'

function StaffLoginPage () {

  const [isAuthenticated, setIsAuthenticated] = useState(!CONFIG.STAFF_PASSWORD)
  const [passwd, setPasswd] = useState(null)
  const [showLoginError, setShowLoginError] = useState(false)

  const doLogin = () => {
    if (passwd === CONFIG.STAFF_PASSWORD) {
      setIsAuthenticated(true)
    } else {
      setShowLoginError(true)
    }
    setPasswd('')
  }

  return (
    <div>
      { (!isAuthenticated && CONFIG.STAFF_PASSWORD) ? (
        <Modal show={true}>
          <div className="modal-content">
            <div className="modal-body">
              { showLoginError ? (
                <div className="alert alert-danger">
                  <i className="fa fa-fw fa-exclamation-triangle"></i> Incorrect password.
                </div>
              ) : null}
              <form>
                <label className="form-label" htmlFor="passwd">
                  Password
                </label>
                <input type="password" className="form-control" id="passwd" value={passwd} onChange={(e) => setPasswd(e.target.value)} />
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary" onClick={doLogin}>
                <i className="fa fa-fw fa-sign-in-alt"></i>
                Submit
              </button>
              <Link to="/home" type="button" className="btn btn-secondary">
                Cancel
              </Link>
            </div>
          </div>
        </Modal>
      ) : null}

      { isAuthenticated ? (
        <StaffPage />
      ) : (
        <div>
          Access Denied
        </div>
      )}
    </div>
  )
}

export default StaffLoginPage
