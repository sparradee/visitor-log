# Things To Do
- **Bug:** Display of normalized text isn't complete; HTML entities like ampersands (&) are still problematic in places - implementation will need to be restarted from the ground up to account for these characters
  - Possible solution: Removed the `.escape()` directive from the API sanitization; might have solved the problem?

## Home Page
- Will eventually need larger sign-in/out buttons for a balanced appearance

## Admin Page
- When updating content and pressing “enter”, screen jumps to Guests tab (?)
  - Note: caused because none of the modal window forms catch the 'enter'

## Tests
- API test cases are outdated and will currently fail

# Wishlist for Future Expansion
- Create a Configuration panel
  - Enable/disable photo feature
- CSV import for employee and/or division databases
- Make html pages and forms responsive (i.e., change layout depending on screen width)
- Figure out a plan to purge soft-deleted visitors (after 30 days?)
- Testing / Dev
  - Document the code properly
  - Complete sourcecode Lint configuration
  - Implement proper console logging
  - Finish creating unit/integration tests
