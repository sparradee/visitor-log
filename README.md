# Visitor Log Application

## Prerequisites

Please note that this application was designed under the assumption it will be run on a Windows system; it *should* run wherever you install it, but some of the configuration options may need to be adjusted.

A Windows system will need the following applications installed before beginning:

- mongodb (https://www.mongodb.com/try/download/community?tck=docs_server)
- Git for Windows (https://gitforwindows.org/)
- node.js (https://nodejs.org/en/download/)

All the instructions in this document are intended to be typed into a command console window - you should open the **Git Bash** application after you've intsalled Git.

## Installation

To install this application, first pull down the sourcecode from the repository by using the following commands:

```sh
git clone https://gitlab.com/sparradee/visitor-log
cd visitor-log
```

Run the below to install the tools required to build this project.

```sh
npm install
```

Run the following command to install all the dependent scripts and libraries that this application will be using. Note that this may take several minutes to complete, depending on your internet speeds.

(*Note*: There may be warning messages during this stage; they should not interfere with the installation process.)

```sh
npm run installApplication
```

# Configuration

There is a file named `config.js` in the home folder of this project; this file houses all the configuration options for the site that will be used while running in development and in production mode.

The simplest way to edit this config file is by using this command:

```sh
nano config.js
```

(Nano if a simple to use text editor that will run inside your command console window. You can, of course, use any editor you prefer.)

The available settings are listed below. These settings **SHOULD BE CHANGED IMMEDIATELY**:

- **staffPassword**: This is the password required for a user to access the staff utilities pages; if left blank, no password will be required

The following settings are also configurable, but the site will function correctly using the default settings:

- **databaseHost**: This is the IP Address or FQDN address of the server that runs the MongoDB database for this application
- **databasePort**: This is the port that the MongoDB server runs on; this probably doesn't need to be changed
- **databaseName**: This is the name of the database on the MongoDB server that will house the Visitor-App data

By default, webcam support is **enabled**. This means that the sign-in page will request access to an attached web camera for taking photos of guests. To disable webcam features, there is the below option.

- **useWebcam**: Set to **true** or **false** to determine is webcam features are enabled

# Running in Production Mode

## Building for Production
To prepare the application for running in production, the code must be compiled property; use the following command to compile the site.

```sh
npm run build
```

## Running the Production server
The below will start the webserver running on port 80 of the local computer; you can then access the site in your browser by going to the url http://localhost/ or http://127.0.0.1/

```sh
npm run prod
```

# Running in Development Mode
The application can be run in development mode via this command:
```sh
npm run dev
```

This will start up a backend server on port 3000, open the front-end site on port 3001, and will automatically open the site in your default browser. (http://localhost:3001/) The command console will show a lot of status and informational output messages on the status of the server as it processes files and data.

If you _not_ have a MongoDB server installed, you can still run the site using an in-memory ('simulated') database. The application will function as intended, however none of the data that is saved will persist between runs of the site. This is for testing/troubleshooting only!

```sh
npm run dev-mem
```

## Application Testing

To verify that all functionality is operating as intended, run the test cases via the following command.

```sh
npm run test
```
