module.exports = {

  // This is the password required for a user to access the Staff administration pages.
  // If you leave it blank (either "", or "", or null) then no password will be required,
  // and all users will be able to access the staff tools.
  staffPassword: "password",


  // The following options configure how this application will connect to a MongoDB server.
  // If your mongo server is running on the same system as this web server, then no additional
  // configuration will be required.
  databaseHost: "127.0.0.1",
  databasePort: "27017",
  databaseName: "visitor-log",

  // If you want to disable the addition of webcam features, set useWebcam to false.
  useWebcam: true

}
