const fs = require('fs')
const siteConfig = require('./config')

/**
 * Build a config.json and place it in
 *    /server/
 *    /server/frontend/src/
 *
 * @param {function} done
 */
function updateConfig (done) {
  const CONFIG = {
    API_PORT: parseInt(process.env.API_PORT || 3000),
    PORT: parseInt(process.env.PORT || 3001),
    STAFF_PASSWORD: siteConfig?.staffPassword || null,
    DB_HOST: siteConfig?.databaseHost || '127.0.0.1',
    DB_PORT: siteConfig?.databasePort || '27017',
    DB_NAME: siteConfig?.databaseName || 'visitor-log',
    WEBCAM: !!siteConfig?.useWebcam
  }

  console.log(`Setting configuration to ${JSON.stringify(CONFIG)}`)

  try {
    fs.writeFileSync('./server/config.json', JSON.stringify(CONFIG))
    fs.writeFileSync('./server/frontend/src/config.json', JSON.stringify(CONFIG))
  } catch (err) {
    console.log(err.message || err)
    done(err)
  }
  done()
}

exports.updateConfig = updateConfig
